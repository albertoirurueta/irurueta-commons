# irurueta-commons
Utility classes for Java SE

[![Build Status](https://travis-ci.org/albertoirurueta/irurueta-commons.svg?branch=master)](https://travis-ci.org/albertoirurueta/irurueta-commons)
[![Coverage Status](https://coveralls.io/repos/github/albertoirurueta/irurueta-commons/badge.svg?branch=master)](https://coveralls.io/github/albertoirurueta/irurueta-commons?branch=master)

[Maven Site Report](http://albertoirurueta.github.io/irurueta-commons)
