/**
 * Copyright (C) 2016 Alberto Irurueta Carro (alberto@irurueta.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.irurueta.commons.units;

/**
 * Paper size.
 * This is typically used when generating reports in PDF format.
 */
public enum PaperSize {
    /**
     * Din A-4 (used in most European countries).
     */
    A4,
    
    /**
     * Letter (used in UK, USA, or countries where imperial units are used).
     */
    LETTER
}
